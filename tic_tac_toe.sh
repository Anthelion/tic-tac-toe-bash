#!/bin/bash
# The classic tic-tac-toe
# Win condition: place three tokens in a line, horizontally or vertically
# The game runs in a loop, which constantly checks if the win condition has been met by either player
# The player can also type in "quit/q" to quit a game

# Converts a coordinate to index
function coordToIdx() {
	let local idx="$1 * $w + $2"
	echo "$idx"	
}

# The Tic-Tac-Toe board, which is a 1D array
board=()
# width
w=3
# height
h=3
# game mode
mode="e"

maxAxisIdx=$(($w - 1))
let maxIdx="$w * $h - 1"
let centreIdx="$maxIdx / 2"
# main diagonal increment value
mdIncrementVal=$((w+1))
# anti-diagonal increment value
adIncrementVal=$((w-1))
finished=false
input=c

function printBoard() {
    local x; local y

    for (( y=$maxAxisIdx; y>=0; y-- )); do
        for (( x=0; x<$w; x++ )); do
            local idx=$((x*w+y))
            local token=${board[$idx]};

            if [[ -z $token ]]; then
                token="*"
            fi

            printf "$token"
            printf " "
        done

        printf "\n"
    done
}

# Returns the value of the corresponding grid at the specified index
function get() {
	local idx=$1
	local token=${board[$idx]}
	echo "$token"
}

function diagonalCheck() {
	local diff=$1
	local token=$2
    # main diagonal count
	local mdCount=0
    # anti-diagonal count
    local adCount=0
	local i
    local mdCheck=-1
    mdCheck=$((diff%mdIncrementVal))

    if [[ $diff -eq 0 ]]; then
        mdCount=$(mainDiagonalCheck $token)
        adCount=$(antiDiagonalCheck $token)

        if [[ $mdCount -gt $adCount ]]; then
            echo "$mdCount"
        else
            echo "$adCount"
        fi
    elif [[ $mdCheck -eq 0 ]]; then
		mdCount=$(mainDiagonalCheck $token)
        echo "$mdCount"
    else
		adCount=$(antiDiagonalCheck $token)
        echo "$adCount"
    fi
}

function mainDiagonalCheck() {
    local token=$1
    local count=0
    local i
    local currentIdx=0;

    while [ $currentIdx -lt $maxIdx ] || [ $currentIdx -eq $maxIdx ]; do
        local val=$(get $currentIdx)

        if [[ $val == $token ]]; then
            (( count++ ))
        fi

        currentIdx=$((currentIdx+mdIncrementVal))
    done

    echo "$count"
}

function antiDiagonalCheck() {
    local token=$1
    local count=0
    local i
    local currentIdx=$((w-1));
    local stopIdx=$((maxIdx-adIncrementVal))

    while [ $currentIdx -lt $stopIdx ] || [ $currentIdx -eq $stopIdx ]; do
        local val=$(get $currentIdx)

        if [[ $val == $token ]]; then
            (( count++ ))
        fi

        currentIdx=$((currentIdx+adIncrementVal))
    done

    echo "$count"
}

function check() {
	local x=$1; local y=$2; local token=$3; local direction=$4; local diff=$5
	local count=0
	local val=*
	local i
	
	if [[ $direction == "d" ]]; then
		# diagonal check
		count=$(diagonalCheck $diff $token)
	else
		for (( i=0; i<=$maxAxisIdx; i++ )); do
			if [[ $direction == "v" ]]; then
				# vertical check
				val=$(get $(coordToIdx $i $y))
			else
				# horizontal check
				val=$(get $(coordToIdx $x $i))
			fi

			if [[ $val == $token ]]; then
				(( count++ ))
			fi
		done
	fi

	echo "$count"
}

function won() {
	finished='true'
	echo "Win!"
	exit 1;
}

function checkWinCondition() {
	local x=$1; local y=$2; local token=$3;
	local flags="110"
    # The diagonal flag. Indicates that diagonal checks are required
	local dFlag="001"
	local selectedIdx=$(coordToIdx $x $y)
	echo "x: $x; y: $y; selectedIdx: $selectedIdx"

	if ([[ $x -eq 0 ]] && [[ $y -eq 0 ]]) || ([[ $x -eq 0 ]] && [[ $y -eq $maxAxisIdx ]]) || ([[ $x -eq $maxAxisIdx ]] && [[ $y -eq 0 ]]) || ([[ $x -eq $maxAxisIdx ]] && [[ $y -eq $maxAxisIdx ]]) || [[ $x -eq $y ]]; then
		let flags="$flags | $dFlag"
	fi
	
	let local hasDFlag="$flags & $dFlag"
	local diff=$(($centreIdx - $selectedIdx))

	# diff is negative, it means that the selected point is beyond the centre square
	# vice versa. If diff is 0, then the selected idx is the index of the centre point
	# in which case, all directions need to be checked
	
	# Horizontal check, looping through y-axis
	local result=$(check $x $y $token "h" $diff)
	if [[ $result -lt 3 ]]; then
		# Vertical check
		result=$(check $x $y $token "v" $diff)
		if [[ $result -lt 3 ]] && [[ $hasDFlag -eq 1 ]]; then
			# Diagonal check. Only needed for corners and centre point
			result=$(check $x $y $token "d" $diff)
		fi
	fi

	if [[ $result -eq 3 ]]; then
		won
	fi
}

# @OBSOLETE
function checkForEmptySquare {
	echo ""
}

function easyAI() {
	local stop="false"; local i; local j

	for i in {0..2}; do
		if [[ $stop == "true" ]]; then
			break
		fi

		for j in {0..2}; do
			local idx=$(coordToIdx $i $j)
			local token=$(get $idx)

			if [[ -z $token ]]; then
				board[$idx]="o"
				printBoard
				checkWinCondition $i $j "o"
				stop="true"
				break
			fi
		done
	done
}

function ai() {
	# The AI is going to have several modes, namingly easy, medium and difficult
	# However, only easy mode is available at the moment. The other two will be released
	# in the future - hopefully.
	echo "----------"
	case $mode in
		e) easyAI
	esac
}

function gameCycle() {
	# Looping through all elements of the array
	for coords in $@; do
		local x=${coords:0:1}; local y=${coords:2:1};
		local idx=$(coordToIdx $x $y)

		if [[ -z ${board[$idx]} ]]; then
			board[$idx]="x"
			printBoard
			checkWinCondition $x $y "x"
			ai
		else
			echo Square is occupied
		fi
	done
}

function start() {
	if [[ -z $mode ]]; then
		modeSelection
	fi

	echo "Game mode: $mode"

	while [ $finished == 'false' ];
		do
		read -p "Please enter a valid coordinate [x,y] (0-indexed): " input
		if [ $input == 'q' ] || [ $input == 'quit' ]; then
			echo "Bye for now!"
			exit 1;
		else
			gameCycle $input
		fi
	done
}

function modeSelection() {
	echo "Please select a game mode: "; printf "Easy (e)\nMedium (m)\nHard (h)"; read -s mode
}
# End of functions

# Entry point of the game
while getopts m: option; do
	case $option in 
		m) mode=$OPTARG;;
		?) modeSelection
	esac
done
start
